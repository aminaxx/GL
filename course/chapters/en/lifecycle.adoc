[[chapter_lifecycle]]
== Lifecycle

[[lifecycle_analysis]]
=== Needs Analysis

This phase consists in the collection of data necessary to report on the actual situation.

It is first necessary to *understand the context*, using all relevant sources of information.
The main source of information is the client, to whom any useful questions should be asked, without fear to go deeper:
some vital elements are sometimes so obvious/implicit to the client that he or she may not think to mention them.
He or she may omit some important details, too.

Then, putting the needs expressed by the client into perspective with the context allows the *real* needs to be redefined or validated.
These real needs should be ordered by degree of importance and can be grouped by themes.

At this phase the different design parameters are also established,
and in particular the *constraints* that may prevent implementing all or part of the original objectives.
Technical or economic constraints are not the only ones to be considered; some areas are also affected
by legal, social, environmental, and so on.

This analysis phase leads to the validation by the <<roles_moa,client>> of a realistic *scope statement* suitable to its real needs.
This document clearly specifies the objectives to be fullfilled.

This phase also allows the <<roles_moe,supplier>> to evaluate that she has access to all the skills needed for the project, or if external resources are required.

==== Example of analysis tools

===== Five W's

To clarify each need, always ask at least these 5 questions: Who, What, Where, When, Why?

An equivalent is:
** 4 questions: Who? What? Where? When?
** Completed by 3 modalities: How? How much ? Why ?

===== Five "Why?"

Asking five questions starting with _Why_ in sequence can help to identify the real cause of a problem,
without giving too much weight to peripheral concerns.

===== Pareto Diagram

Such a diagram graphically represents the different causes of a problem, ordered by their importance.

It is an application of the 80/20 law (or Pareto principle): 20% of the causes produce 80% of the effects.

image::lifecycle/pareto.png[caption="Example of Pareto diagram:", 500px, title="Causes of lateness of students", alt="Ninjas? What! Where?"]

===== Ishikawa Diagram

Also called 5M, fishbone diagram, cause-and-effect diagram, or Fishikawa, this diagram is used to relate causes and consequences.

* Materials involved: raw, consumables, information
* Machine: equipment, hardware, software, technologies
* Method of operation, logic, processes
* Man / Mind power: persos, knowledges
* Medium: environment, context, measurements, inspection

image::lifecycle/ishikawa.png[caption="Example of Ishikawa's diagram:", 500px, title="Causes of abnormally high fuel consumption", alt="Causes of abnormally high fuel consumption"]


[[lifecycle_specification]]
=== Specification

During the specification phase, the needs that were analyzed previously is described with more details,
in the form of *requirements* that the solution must imperatively satisfy.

A specification document can be of two main types:

* A *functional* specification describes the *business processes* in which the solution is involved.
  For example, the units used, the rules for calculation or interaction, etc.
  The functional specification represents the *goal to be achieved*.
* A *technical* specification describes the *technical environment* in which the solution operates.
  For example, the architectural design, the data format to be used for exchanges with the components already in use,
  the programming languages used, the database format, the host system, ...
  can be fixed in this document.
  The technical specification represents the *means to achieve the goal* set by the functional specification.

This phase often leads to more than one specification document.
In particular, the requirements can be further refined during this phase
to a satisfactory level of detail:
one can then create general specifications, and then more detailed ones.

Most often, it is the <<roles_moa,customer>> which is at the origin of the general specifications.
However, it can be relevant for the detailed specifications to be written by the <<roles_moe,supplier>>, with the validation of the customer.

Since it describes to future users and developers what the finished product will look like,
the specification allows cost and delays estimates to be made.
It therefore serves as a basis for establishing the project schedule.

The specification also serves as a contractual basis.
After this phase, all the functionalities that are out of specification
theorically cannot be requested by the customer.

[[lifecycle_conception]]
=== Design

Whereas the specification phase aims to describe the solution seen from the outside,
the design phase describes it from the inside.
While the specification phase describes the constraints, design provides the solutions.

This is the work of the <<roles_moe,supplier>>.

Like any documentation, it can be refined more and more, for example
into a preliminary/architectural design document, and then detailed.

[[lifecycle_implementation]]
=== Implementation / Development

This phase is the *realization* of the solution as it was conceived.

[[lifecycle_tests]]
=== Tests

Testing the software as it is implemented is intended to improve quality
or to know the <<chapter_quality,degree of presence>> of a particular quality.

A *test* is the _partial_ verification of the software.
It corresponds to the combination of three things:

* input data
* object to be tested
* expected situation

If the expected situation corresponds to the situation observed during the test,
it's a sign of good quality for the software.

==== Integration

During the *integration* phase, each software module is integrated with the others and tested as a whole.

Also called *functional testing*, this phase aims to verify the functional aspect of the software
(including performance, stability, etc), which are sometimes not detectable by lower level tests.

==== Acceptance

During the *acceptance* phase, the entire system is tested,
and in an environment as close as possible to the production environment.
The objective is to assess its compliance with the specified requirements.

A particular type of acceptance testing is *factory acceptance* (FAT).
It can take place in the presence of all the actors.
It often precedes an important milestone in the life of the project, such as a delivery.

[[lifecycle_delivery]]
=== Deployment

The deployment of a software consists of its *delivery into production*,
_ie._ making it available to and usable by the customer, as well as the end users.

This phase can be broken down into several steps that will (or will not) apply to a particular project.

* Delivery (_release_, _packaging_) +
  The various components of the solution are prepared in order to make them usable.
* Activation (_install_) +
  The solution is made usable in its production environment.
  Its various components are installed and configured.
* Deactivation (_uninstall_) +
  A previous solution may have to be completely or partially uninstalled,
  or disabled to allow the new solution to replace it.
* Update +
  The new solution may require a newer version of existing dependencies in its production environment.
  It can also be part of a larger system, which must then be updated to allow activation of the solution.

The deployment of the same solution can be done several times.
This phase is made a lot easier by using of a *version manager*, as well as a *configuration manager*.

[[lifecycle_maintenance]]
=== Operation / Maintenance

A software can evolve even after it has been delivered, requiring *maintenances*.

A maintenance can be of several types:

* The *corrective maintenance* consists in resolving an observed anomaly.
** _curative maintenance_ +
   It corrects the anomaly permanently.
** _paliative maintenance_ +
   It prevents the anomaly from damaging the system or the client environment,
   while allowing the software to continue to fullfill all or part of its functionality.
   However, since its impact is necessarily negative to some degree,
   this type of maintenance is often of a temporary nature.
* *Preventive maintenance* consists in operating the software with the specific aim of preventing an anomaly to occur.  This type of maintenance can be _systematic_ (_eg._ daily reboot) or _conditional_.
* *Evolutive maintenance* allows to better respond to the need or to meet new needs,
  by modifying existing software or developing new functionalities.

Maintenance differs from the other phases in that the software in question is already in production.



== Development models

A development model organizes the software construction activities in a structured way.
The detail of the activities that will take place depends on the project.

=== Characteristics

* Clearly defined and implemented
* Understandable by all actors of the project
* Accepted by all actors of the project
* Externally observable (by other actors, stakeholders, ...)
* Should allow to detect problems before production phase.
* A single unforeseen problem shouldn't stop the whole project

Within a development model, each activity must detail:

* The tasks to be carried out and by whom
* Decisions to be taken (if any)
* Artifacts, *deliverables*:
** Documents
** Sources
** Binaries (executables, libraries, ...)

There is at least one start activity and one end activity.
There must be a path linking each activity to the end activity.

Two activities are separated by at least one artifact to be delivered.
An activity cannot start until its input artifacts exist.



[model_waterall]]
=== Waterfall model

This linear model is based on two ideas:

* modifying a step has consequences on the following steps, and therefore
* a step cannot be started before the previous one is completed

This model has a *fixed and predefined* number of phases.
Each phase produces a number of deliverables, also defined in advance.
Each phase begins and ends on a fixed date.
Only when the deliverables of the current phase have been validated can the project move to the next phase.

If an anomaly is detected, one or more phases are rolled back.

image::lifecycle/waterfall.png[caption="Figure 01:", title="Waterfall", alt="Waterfall model"]

This model assumes that most of the requirements are known and mastered at the start of the project.

It requires very important attention to *documentation*.
In particular, each document must be properly delivered,
then wait for feedback and comments on this delivery,
and then improve these documents to incorporate comments,
and so on, until each document is accepted by all parties
... At every step.

==== Benefits

Everything is *predictable*:
The actors know exactly what is to be delivered, when and what it entails.

==== Disadvantages

* The *time needed* to get a testable software is important.
* The most *risky* phases (tests ...) come *at the end* of the cycle.
  This model is therefore in fact not very tolerant to errors.
** What happens if a need has been misinterpreted?
** What if a design detail proves to be unsuitable during implementation or deployment?
* The lifespan of a project is often several years.
  However, this model is very *intolerant* to changes.
** What happens if the need changes?
** What if the nature of the market changes?

==== Applications

However, this model can be suitable in some cases:

* Areas where it is impossible or very expensive to go back.
  For example, building construction gave birth to this model.
  (a building cannot be constructed before the land has been "specified" and the plans "designed").
* Projects with a small perimeter and very short duration.
  In such small projects, the risk of backtracking is presumably low.

It is not recommended for new systems because of the numerous specification problems
and design that novelty brings.



[[model_v]]
=== "V" model

This linear model attempts to highlight the *complementarity* between some phases.
Each study and analysis phase is *coupled* with a test phase that validates it.

image::lifecycle/v.png[caption="Figure 02:", title="\"V\" model", alt="\"V\" model"]

This model is basen on conventions in industrial production.
It is therefore by far the most widely used in the *industrial field*.


==== Advantages

* *Proven* model:
** Extensive use in companies since the 80's
** Supported by many standards
** Supported by numerous tools
* The test phases (_ie._ the "ascending" branch of the V) are as important as the reflection phases (_ie._ the "descending" branch).
  In particular, it is easier to describe exhaustively how to test a feature at the time it is designed.
  The synergy between the analysis of a feature and the description of the test that will attest its quality is beneficial.
* Since it promotes hierarchical and functional decomposition, it allows the work organization, teams and cost control (example: the COCOMO method).
  This gives good visibility.
  Project monitoring is made easier.

==== Disadvantages

* Only minimizes the drawbacks of the <<model_waterfall,waterfall model>> on which it is based.
  The main problem remains the *lack of flexibility*.
* There is a world of difference between theory and practice.
  Phases during which the level of detail is increased,
  especially those for detailed specification and implementation,
  sometimes make it possible to realize that the analyses resulting from the previous phases
  are incomplete or simply not doable as they stand.

==== Applications

Its persistent drawbacks despite its high rate of use make the V-cycle more of an "ideal" that some would like to strive for but is not always applied as is.

It is therefore generally used for large industrial projects with varying degrees of success.

image::lifecycle/v-real.png[caption="Figure 03:", title="What \"V\" model sometimes really means", alt="Imbalance and backtracking"]



[[model_spiral]]
=== Spiral model

Inspired by the Deming Wheel, this iterative model focuses on *risk management*.
It takes the steps of the <<model_v,"V" model>>, but creats successive versions during *cycles* of development (_ie._ iterations).

Each cycle can be divided into 4 distinct stages
represented by the acronym PDCA (_Plan-Do-Check-Act_) :

* Plan
** Needs analysis
** Determination of objectives
** *Risk Analysis*
** Study of alternatives
* Develop
** Specification
** Design
** Implementation
** Unit tests
* Check
** Integration
** Validation
* Act / Adjust
** Delivery
** Definition of the next cycle

image::lifecycle/wheel_deming.png[caption="Figure 04.1:", title="Deming wheel", alt="Deming wheel"]

image::lifecycle/spirale.png[caption="Figure 04.2:", title="Example of spiral development", alt="Spiral pattern"]

Each cycle does not necessarily rely on the artifacts delivered by the previous cycle:
different risks can be addressed one after the other.

As in any iterative model, the number of cycles is not determined in advance.

==== Advantages

* Risk control: we focus on the most uncertain aspects of development.
* Focus on quality.
* Integrates maintenance and development.
* Possibility to invert the order of some independent cycles.
* Compatible with many existing approaches and tools.

==== Disadvantages

* Requires real and complete expertise in the field of risk assessment:
  the nature of the risks may be different from one cycle to another.
* Requires a great deal of experience to set up this rather complex model.
* The first cycles of the spiral generally do not produce a workable solution.
* Verbosity is unsuitable for small projects or areas that are sufficiently well known.
  In the worst case scenario, the strict application of this model (especially risk assessment) is more costly than the project itself.

==== Applications

This model is logically appropriate for projects where the scope is poorly controlled and the risk is significant.



[[model_explore]]
=== Exploratory

The principle of this iterative model is to refine the project specification, as well as its development and validation, in successive iterations.
The objective is to *collaborate* with the customer and to *refine* the solution more and more.

image::lifecycle/explore.png[caption="Figure 05:", title="Exploratory model", alt="Multiple intermediate versions"]

In general, it is the time frame given to the project that decides the number of iterations.

==== Benefits

* Minimizes the risk for new applications: it is possible to explore certain specificities of the system, to evaluate them in order to choose the best strategy.
* The result is often fully understandable and satisfying for the customer.
* Theoretically deliverable at each iteration:
** Favors intermediate tests and validations.
** It is possible to stop the process at any time.

==== Disadvantages

* Temptation to shorten the process and settle for an incomplete solution.
* Lack of structure, therefore impossible to apply "as is" on a large scale.
* Low visibility for external stakeholders.

==== Applications

The preferred field of application of this model is therefore small interactive systems (or parts of a system) whose results can be seen quickly.
In particular, graphical interfaces (GUIs, website mock-ups, ...).



[[model_evolutive]]
=== Evolutive model

This iterative model aims to quickly produce a provisional version of the solution to known needs in order to put it into operation as quickly as possible.
New versions will then be deployed, each one replacing the previous version.
Each version will bring new functionalities or modify existing ones.

Intermediate versions are made by including all the usual phases of a project (design, development, testing, ...) and with all the quality principles of a final version:
they are _not_ disposable prototypes !

==== Incremental model

The first version is a partial system.
Each new version adds a complete new functionality.

image::lifecycle/iterative-vertical.png[caption="Figure 07.1:", title="Incremental, by vertical prototyping", alt="Vertical Prototyping"]

=== Iterative model

The system must be divided into well-defined functionnalities right from the start.
The first version is a complete shell of the system.
Each feature that is not implemented is replaced by a *<<test_object_stub,stub>>*.
Each new version modifies or improves a feature.
Each stub is progressively replaced by the corresponding finished feature.

image::lifecycle/iterative-horizontal.png[caption="Figure 07.2:", title="Scalable, by horizontal prototyping", alt="Horizontal prototyping"]

==== Benefits

* Increases *competitiveness* by reducing time to market.
* Early user training.
* Early detection of contingencies.

==== Disadvantages

* The deployment process of each version must be mastered.
* Risk of leading to a heterogeneous adoption from users:
** Ensure the traceability of each version.
** Ensure the traceability of each of their components.
* The way to obtain feedback from users must be controlled.
* Risk of questioning the kernel ensuring the basic functionalities.

===== Applications

This model is appropriate for any *strongly competitive* field,
and where users are willing to use an incomplete product.



=== Agile

In software engineering, the term *agility* covers several distinct development models,
all of which offer at least the following three characteristics:

* *iterative*: everybody works in periods of varying length (a few weeks);
* *incremental*: each iteration is based on the results of the previous ones;
* *flexible*: each iteration is planned only at the beginning, not at the launch of the project.

The principles of agility have been formalized in the _Agile Manifesto_ (2001).

Various implementations of these principles have been made:
_RAD_, _Crystal Clear_, *unified* processes (Y model or _2TUP_, _RUP_, _AUP_, _XUP_), ...
However, the most used implementations seem today
<<lifecycle_xp,_Extreme Programming_ (_XP_)>> and <<<lifecycle_scrum,_Scrum_>>.

The agile model sees the product as the sum of its *features*.
Each iteration (or *sprint*) produces a number of features.

The process stops when the set of implemented features satisfies the customer.
The final number of features is not known in advance;
on the contrary, the situation is examined at the end of each iteration.

* Expression of need. This need may vary at each iteration.
* Iteration sequence.
  Each iteration must focus on the essential;
  which functionalities are essential for this iteration depends on the expressed need.
** Joint planning between the customer and the development team.
*** Deciding which features will be developed, and which ones will be left out.
*** Translating the requirements into technical language.
** Development: implementation of what has been specified.
*** Implementation
*** Tests
** Inspection
*** Customer validation: check that the result of the iteration (the _artefacts_ delivered)
    is consistent with the expressed need.
*** Deployment: making available to the client the artifacts that were validated.
*** Optionnaly, evaluation: understanding the current status of the project
**** Analysis of difficulties encountered
**** Improvement plan.

==== Benefits

* Pragmatism: each iteration focuses on the essential.
  This process avoids wasting time on tasks with no added value for the client.
  This allows to get closer to an optimal operating mode.
* Reactivity offered by short iterations.
* Focus on customer satisfaction.

==== Disadvantages

* The cost in time for the customer is not to be neglected.
* Not as easy to set up as it seems.
* How to justify tasks necessary to the organization but without immediate benefit to the customer?
* An astonishing number of organizational or personal obstacles can appear.
  This working method can indeed be difficult to approach for some people.
  (resistance to change, communication errors, etc).
* Difficult to adapt to teams that are too large, suffering from too high turnover,
  or composed of members who are too specialized in their respective fields.
* Difficult to adopt if the product code is poorly mastered or insufficiently tested.

==== Applications

Models implementing this process require the customer's need to be clearly expressed.
It is suitable for companies that are open-minded and where inter- and intra-team communication is good.



==== Two examples of agile models

_Extreme Programming_ (_XP_) and _Scrum_ are today the two best known agile methods used in companies.
They are compatible with each other.

[[agility_backlogs]]
===== Planning

Both _XP_ and _Scrum_ make extensive use of "TODO lists" called *backlogs*.

* The *_Product backlog_* is a list of all activities that must be completed in order for the project to be completed.
  The activities are sorted in order of priority.
* The *_Sprint backlog_* is the list of activities to be performed in the current sprint.
  At the beginning of each sprint, this list is populated with activities from the product backlog,
  during a meeting called the _planning poker_ (or _planning game_),
  which brings together the development team but also its client.
  Generally, the highest priority activities are chosen.
  The expected duration of each activity placed in the sprint backlog must be estimated (quantified).
  This duration is represented in the form of an abstract "cost" of the activity.
  compared to the other activities already carried out in previous sprints. +
  The sum of the costs of the activities performed in the previous sprints gives the *velocity*
  of the development team.
  Based on its previous velocities, the development team can realistically determine
  how many activities can be expected to be performed in the sprint that is starting.
* The _"Doing"_ list includes activities that are in progress.
  It is important to get the activities out of the sprint backlog in this way so that several people
  are not working on the same thing without knowing it.
* The _"Done"_ list is the current sprint activities that have been completed.



[[lifecycle_xp]]
===== Extreme Programming (XP)

_XP_ is as much a style as it is a development discipline.
It is also a path of *continuous improvement*.
As its name implies, this method is almost exclusively focused on software development.
Above all, it is an assortment of simple principles that, combined and pushed to the extreme,
allow for an excellent level of software quality to be achieved.

====== Principles

* Recognizing that speaking a common vocabulary is the most effective way to understand each other,
  _XP_ encourages the definition and use of the same *metaphors* by the different actors.
* Recognizing that the best way to save time is to keep things simple,
  a *simple solution* must always be preferred over a complicated solution.
* Recognizing that customer needs are changing, *short iterations* are to be preferred.
* Recognizing that <<tests_types,tests>> are useful, *systematic* tests should be developed and executed,
  especially by practicing <<tdd,TDD>>.
* Recognizing that integration is a crucial phase,
  it is essential to make <<continuous_integration,*continuous integration*>>.
* Recognizing that code review is useful, it will be done systematically, via *pair programming*.
* Recognizing that a good software architecture is essential, the design will be continuously improved,
  via the practice of systematic *refactoring*.

[[tdd]]
====== Test Driven Development (TDD)

TDD is a development discipline that requires systematic and comprehensive writing
of code testing the functionality before developing the functionality itself.
In addition to not "forgetting" to test what you implement,
this discipline also offers the advantage of leading to better software design,
which stems from the fact that, right from the start, the code is viewed from the user's / caller's perspective.

Here is the typical workflow for a developer working in TDD:

. Write a test
. Run the test, and verify that it fails.
. Write the source code necessary and sufficient for the test to pass.
. Verify that the test passes.
. <<refactoring,Refactor>> the source code.

====== Refactoring

The discipline of refactoring consists in rethinking/rewriting the code in order to improve it.
In particular, refactoring code consists in giving the necessary attention to the following points:

* Does someone reading this code have all the necessary information in one place
  to properly understand the code?
  Among other things:
** Why was this code written ?
** What are its inputs and outputs?
** What are the algorithmic principles involved?
** Are the code comments, if present, synchronized with the source code?
* Is any redundancy removed?
** Factorizing code to avoid duplication.
** Removing dead code.
** Removing unnecessary comments.
* Are the algorithms written in the most understandable way possible?
** Extraction of named methods and possibly descriptive comments.
** Formatting of the code.
* Is the <<solid_principle,best compromise>> made between minimum number of classes
  and complexity specific to each class?



[[lifecycle_scrum]]
===== Scrum

_Scrum_ is initially an empirical method of organizing and monitoring the project.
It has the particularity of letting the development team self-organize,
which makes it compatible with, for example, _Extreme Programming_.

Its values are those of *transparency* (towards all parties involved, and through a common language),
*inspection* of deliverables, and *adaptability*.

====== Rituals

Scrum includes certain meetings, or rituals, to promote these values:

* the *planning meeting* (or _planning poker_) that takes place at the beginning of the sprint.
* the *daily meelee* (or _standup meeting_), which brings together all the team members on a daily basis
  for a maximum of 15 minutes, and offers everyone the opportunity to quickly entrust to others :
** what they have been doing since the last scrimmage
** what they plan to do between now and the next scrum
** where applicable, the problems they have encountered or anticipates the team may encounter in the future
* the *sprint review* is product-focused, bringing together all pertinent stakeholders at the end of each sprint,
  with the objectives of :
** validate the increment brought to the product by the sprint
** update the various <<agility_backlogs,backlogs>>
* the *retrospective* is specific to the development team, and allows them to go back over the last sprint(s).
  The objective is *continuous improvement*.

====== Roles

_Scrum_ recognizes exactly three roles (no more, no less) for members of a project team.

* The *product owner* a single person who is the owner of the product, and the client of the development team.
  It is the product owner who holds the *vision* of what the product should be.
  The product owner also owns the <<agility_backlogs,product backlog>> : 
  they sets the priority between the different activities and makes sure their specification is understood by all.
  Scrum recommends that the product owner be *always* present on the same site as the development team,
  who may need their feedback or explanations at any time during the sprint.
* Members of the *Development Team* are collectively responsible for the delivery of the sprint,
  both in terms of quality and deadlines.
  They are the ones who estimate the cost of each activity during _planning poker_. +
  Each member of the development team is considered *non-specialized* / multidisciplinary.
  This means that any member can act on each and any activity of the _sprint backlog_. +
  The development team operates in a self-organizing manner: _Scrum_ does not impose anything in terms of the way developments are carried out; it simply provides a framework for quality and deadlines to be met. +
  The development team is "single-product" and only takes orders from the product owner.
  _Scrum_ recommends a size of 3 to 9 people on the development team.
* The *scrum master* is responsible for the adherence of everyone to the _Scrum_ method and its implementation.
  He has the role of *facilitator*, *trainer* and *coach*. +
  He is also the one who participates in the "scrum de scrums".
