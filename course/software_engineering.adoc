= Software Engineering
Régis WITZ <rwitz@unistra.fr>
:doctype: book
:toc: left
:toc-title: Table of contents
:toclevels: 2


include::chapters/en/intro.adoc[]

include::chapters/en/quality.adoc[]

include::chapters/en/lifecycle.adoc[]

include::chapters/en/pert.adoc[]

include::chapters/en/uml.adoc[]

include::chapters/en/tests.adoc[]

include::chapters/en/ci.adoc[]

include::../practical/en/master_list.adoc[]
